// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// Provides a mixin to add a hidden property and a toggle() method to an object. The module returns
// the function that may be used to make an object hideable.
//
// Usage:
//
//    require ('utilities/hideable').call (this, initial);
//
// The initial parameter will be the initial value of the hidden property. It defaults to false.

define (function (require) {

   'use strict';

   var ko = require ('knockout');

   var makeHideable = function (initial) {

      var self = this;

      self.hidden = ko.observable (arguments.length === 0 ? false : initial);
      self.toggle = function () { self.hidden (! self.hidden ()); };

   };

   return makeHideable;

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////