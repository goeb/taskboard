// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// Utility functions. The module returns an object with the functions as properties. These are:
//
//    getTaskList()              - Get a task list instance by its ID.
//    getTaskStage()             - Get a stage instance by its ID.
//    getTaskItem()              - Get a task item instance by its ID.
//
//    isEvent(param)             - Checks if the parameter is a jQuery event instance.
//
//    placeCaretAtEnd(element)   - Place the text cursor at the end of some input element.
//    selectText(element)        - Selects all text of the element.
//
// Usage:
//
//    var utils = require ('utils');
//    utils.placeCaretAtEnd (element);

define (function (require) {

   'use strict';

   var jQuery = require ('jquery'  );
   var ko     = require ('knockout');
   var model  = require ('model'   );

   // Returns true if the supplied parameter looks like a jQuery (!) event, else false.

   var isEvent = function (event) {
      return (
         event
         && typeof event === 'object'
         && 'originalEvent' in event
         && 'relatedTarget' in event
         && 'target'        in event
         && 'type'          in event && event.type
      );
   };

   // Returns a list/stage/item instance. The first parameter is the name of the board's method used
   // to look up the instance. The second parameter is either an array with the instance's ID as
   // second element or a jQuery.event instance with the target set to the element from which the
   // requested instance can be retrieved using Knockout's dataFor() function. This function is not
   // exported directly, see the returned object for details.

   var getStuff = function (method, params) {
      if (isEvent (params)) {
         return ko.dataFor (params.target);
      }
      else {
         return model.board [method] (parseInt (params [1], 10));
      }
   };

   // Place the text cursor at the end of the text in an input (or contenteditable) element. Expects
   // the DOM element as first parameter (this must be the DOM object, not a jQuery object).
   //
   // Example with jQuery (using array access to get the DOM element):
   //
   //    placeCaretAtEnd (jQuery ('#myinput') [0]);

   var placeCaretAtEnd = function (element) {
      element.focus ();
      if (typeof window.getSelection != 'undefined' && typeof document.createRange != 'undefined') {
         var range = document.createRange ();
         range.selectNodeContents (element);
         range.collapse (false);
         var sel = window.getSelection ();
         sel.removeAllRanges ();
         sel.addRange (range);
      }
      else if (typeof document.body.createTextRange != 'undefined') {
         var textRange = document.body.createTextRange ();
         textRange.moveToElementText (element);
         textRange.collapse (false);
         textRange.select ();
      }
   };

   // Selects all text inside an element. Usage is the same as for placeCaretAtEnd(), see above.

   var selectText = function (element) {
      element.focus ();
      if (typeof window.getSelection != 'undefined' && typeof document.createRange != 'undefined') {
         var range = document.createRange ();
         range.selectNodeContents (element);
         var sel = window.getSelection ();
         sel.removeAllRanges ();
         sel.addRange (range);
      }
      else if (typeof document.body.createTextRange != 'undefined') {
         var textRange = document.body.createTextRange ();
         textRange.moveToElementText (element);
         textRange.select ();
      }
   };

   // Return the object containing the utility functions:

   return {
      getTaskItem     : getStuff.bind (null, 'getItem' ),
      getTaskList     : getStuff.bind (null, 'getList' ),
      getTaskStage    : getStuff.bind (null, 'getStage'),
      isEvent         : isEvent,
      placeCaretAtEnd : placeCaretAtEnd,
      selectText      : selectText,
   };

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////