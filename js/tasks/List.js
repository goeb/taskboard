// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// This module returns the constructor for task list objects. A list has the following properties:
//
//    stages                  - Observable array. Contains all stages of the task list.
//    name                    - Observable, getter/setter. Name of the task list.
//    board                   - Parent task board of the task list, null by default.
//
//    collapsed               - Observable, getter/setter. Whether the list is collapsed.
//    toggleCollapsed()       - Toggles the collapsed state between true and false.
//
//    editName                - Observable, getter/setter. Whether it is currently being edited.
//    toggleEditName()        - Toggles the editName state between true and false.
//
//    id()                    - Returns the ID of the task list. Read-only.
//
//    addStage(name,id)       - Adds and returns a stage with the specified name and ID (optional).
//    removeStage(stage)      - Remove the stage (specified by its instance).
//    clear()                 - Remove all stages from the list and dispose of all subscriptions.
//
//    stageMoved(arg,even,ui) - Helper function to be used with the sortable binding.
//
//    toSimpleObject()        - Returns an object representing the current state of the instance.
//
// The following methods are added to the prototype:
//
//    getNextId()             - Returns the ID that will be automatically used for the next list.
//    setNextId(id)           - Sets the ID that will be automatically used for the next list.
//
//    fromSimpleObject(obj)   - Returns a list instance based on the state of the object.
//
// Usage:
//
//    var list = new (require ('tasks/List')) ();

define (function (require) {

   'use strict';

   var ko     = require ('knockout'   );
   var Stage  = require ('tasks/Stage');
   var nextId = 1;

   var List = function (name, setId) {

      var self = this;
      var id   = setId;

      // Set the ID to the next value unless it is already set. If it is supplied as a parameter to
      // the constructor, the value of nextId will not be changed unless it is below the supplied
      // value, in which case it will be set to this value + 1.

      if (id) {
         if (id >= nextId) {
            nextId = id + 1;
         }
      }
      else {
         id = nextId ++;
      }

      self.id              = function () { return id; };
      self.board           = null;
      self.stages          = ko.observableArray ();
      self.name            = ko.observable (name );
      self.collapsed       = ko.observable (false);
      self.editName        = ko.observable (false);
      self.toggleCollapsed = function () { self.collapsed (! self.collapsed ()) };
      self.toggleEditName  = function () { self.editName  (! self.editName  ()) };

      // Creates a new stage with the specified name and adds it to the list. Returns the new stage
      // instance. Note: The stage ID will be selected automatically unless specified.

      self.addStage = function (name, id) {
         var stage = new Stage (name, id);
         stage.list = self;
         self.stages.push (stage);
         return stage;
      };

      // Removes the stage (specified by the instance) from the list. This method will cancel all
      // of the stage's subscriptions and remove all of the stage's content before removing the
      // stage itself.

      self.removeStage = function (stage) {
         stage.clear ();
         self.stages.remove (stage);
      };

      // Subscribe to changes in the stages list, and notify the task board of all additions and
      // deletions (requires the board property to be set to the parent instance). To unsubscribe,
      // call the clear() method (may be a good idea before deleting a list).

      var stagesSubscription = self.stages.subscribe (function (changes) {
         if (! self.board) {
            return;
         }
         changes.forEach (function (change) {
            if (change.hasOwnProperty ('moved')) {
               return;
            }
            if      (change.status === 'added'  ) { self.board.stageAdded   (change.value); }
            else if (change.status === 'deleted') { self.board.stageDeleted (change.value); }
         });
      }, null, 'arrayChange');

      // Clear the list content and unsubscribe from all changes. This should be called before
      // deleting a list.

      self.clear = function () {
         self.stages ().forEach (function (stage) {
            stage.clear ();
         });
         self.stages.removeAll ();
         stagesSubscription.dispose ();
      };

      // Called after drag & drop moving a task stage (using the sortable binding). Update the list
      // property of the stage to the new one. See <https://github.com/rniemeyer/knockout-sortable>.

      self.stageMoved = function (arg, event, ui) { arg.item.list = ko.dataFor (event.target) };

      // Return simple object for saving (in this case all properties except the board property,
      // since circular references will cause a problem when converting to JSON):

      self.toSimpleObject = function () {
         return {
            id        : self.id        (),
            name      : self.name      (),
            collapsed : self.collapsed (),
            stages    : self.stages ().map (function (stage) { return stage.toSimpleObject () }),
         };
      };

   };

   // This returns a new list instance based on the state of the specified object (one returned by
   // the toSimpleObject() method). All properties except editName will be restored. Expects the
   // object as first parameter, the parent task board instance as second parameter.

   List.fromSimpleObject = function (obj, board) {
      var list = new List (obj.name, obj.id);
      list.board = board;
      list.collapsed (obj.collapsed);
      obj.stages.forEach (function (stageObj) {
        list.stages.push (Stage.fromSimpleObject (stageObj, list));
      });
      return list;
   };

   // Get or set the ID used for the next list instance:

   List.getNextId = function ()   { return nextId };
   List.setNextId = function (id) { nextId = id   };

   // Return the constructor:

   return List;

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////