// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// This module returns the constructor for the board object. A board has the following properties:
//
//    lists                   - Observable array. Contains all task lists of the board.
//    length()                - Computed observable, returns the number of lists on the board.
//
//    addList(name,id)        - Add a new task list with the specified name and ID (optional).
//    removeList(list)        - Remove the specified list instance from the board.
//    clear()                 - Clears all the content from the board. Resets list/stage/item IDs.
//
//    unsubscribe()           - Clean up subscriptions. Should be called before a board is deleted.
//
//    listAdded(list)         - Automatically called when a new list is added.
//    stageAdded(stage)       - Called from list instances automatically.
//    itemAdded(item)         - Called from stage instances automatically.
//
//    listDeleted(list)       - Automatically called when a list is deleted.
//    stageDeleted(stage)     - Called from list instances automatically.
//    itemDeleted(item)       - Called from stage instances automatically.
//
//    getList(id)             - Return the task list with the specified ID.
//    getStage(id)            - Return the task stage with the specified ID.
//    getItem(id)             - Return the task item with the specified ID.
//
//    toSimpleObject()        - Return an object representing the current state of the instance.
//    fromSimpleObject(obj)   - Restores the state of the instance to the one represented by obj.
//
// Usage:
//
//    var board = new (require ('tasks/Board')) ();

define (function (require) {

   'use strict';

   var ko    = require ('knockout'   );
   var List  = require ('tasks/List' );
   var Stage = require ('tasks/Stage');
   var Item  = require ('tasks/Item' );

   var Board = function () {

      var self      = this;
      var allLists  = {};
      var allStages = {};
      var allItems  = {};

      // The actual array of task lists for the board, and a computed observable returning the
      // number of lists:

      self.lists  = ko.observableArray ();
      self.length = ko.computed (function () { return self.lists ().length });

      // Creates a new task list with the specified name and adds it to the board. Returns the new
      // task list instance. Note: The list's ID will be selected automatically unless specified.

      self.addList = function (name, id) {
         var list = new List (name, id);
         list.board = self;
         self.lists.push (list);
         return list;
      };

      // Removes the list (specified by the instance) from the board. This method will cancel all
      // of the list's subscriptions by calling list.unsubscribe() and remove all the list content
      // before removing the list itself.

      self.removeList = function (list) {
         list.clear ();
         self.lists.remove (list);
      };

      // Subscribe to changes in the task lists. To unsubscribe, call the unsubscribe method (may be
      // a good idea before deleting a board). This will automatically call unsubscribe() for all
      // the task list instances.

      var listsSubscription = self.lists.subscribe (function (changes) {
         changes.forEach (function (change) {
            if (change.hasOwnProperty ('moved')) {
               return;
            }
            if      (change.status === 'added'  ) { self.listAdded   (change.value) }
            else if (change.status === 'deleted') { self.listDeleted (change.value) }
         });
      }, null, 'arrayChange');

      // Dispose of the manual subscription. This and the clear() method should be called before a
      // board is deleted (maybe because a new board instance is bound to the DOM), usually this is
      // not needed.

      self.unsubscribe = function () {
         listsSubscription.dispose ();
      };

      // Clear the board content. This will basically reset the board. It will remove all task lists
      // and cancel all the subscriptions, and it will reset all IDs. It will not cancel the board's
      // own subscription for the lists changes

      self.clear = function () {

         // Clear the list and get rid of the subscriptions:
         self.lists ().forEach (function (list) {
            list.clear ();
         });

         // Remove all lists:
         self.lists.removeAll ();

         // Reset all IDs:
         List.setNextId  (1);
         Stage.setNextId (1);
         Item.setNextId  (1);

         // And delete all references to lists/stages/items:
         allLists  = {};
         allStages = {};
         allItems  = {};

      };

      // Keep a list of all lists/stages/items by their ID. These methods (except the listAdded and
      // listDeleted methods) are supposed to be called from the list and stage instances from their
      // own subscriptions. See the List and Stage classes for details.

      self.listAdded    = function (list ) { allLists  [list.id  ()] = list  };
      self.stageAdded   = function (stage) { allStages [stage.id ()] = stage };
      self.itemAdded    = function (item ) { allItems  [item.id  ()] = item  };

      self.listDeleted  = function (list ) { delete allLists  [list.id  ()] };
      self.stageDeleted = function (stage) { delete allStages [stage.id ()] };
      self.itemDeleted  = function (item ) { delete allItems  [item.id  ()] };

      // Get a list/stage/item by its ID:

      self.getList  = function (id) { return allLists  [id] };
      self.getStage = function (id) { return allStages [id] };
      self.getItem  = function (id) { return allItems  [id] };

      // Return simple object (i.e. no observables, no circular references) for saving:

      self.toSimpleObject = function () {
         return {
            lists       : self.lists ().map (function (list) { return list.toSimpleObject () }),
            nextListId  : List.getNextId  (),
            nextStageId : Stage.getNextId (),
            nextItemId  : Item.getNextId  (),
         };
      };

      // Restore the state of the instance to the one returned by toSimpleObject. This will clear
      // all the current content of the board:

      self.fromSimpleObject = function (obj) {
         self.clear ();
         obj.lists.forEach (function (listObj) {
            self.lists.push (List.fromSimpleObject (listObj, self));
         });
         List.setNextId  (obj.nextListId );
         Stage.setNextId (obj.nextStageId);
         Item.setNextId  (obj.nextItemId );
      };

   };

   return Board;

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////