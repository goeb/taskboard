// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// This module returns the constructor for task stage objects. A stage has the following properties:
//
//    items                   - Observable array. Contains all task items of the stage.
//    list                    - Parent task list of the stage, null by default.
//
//    addItem(name,id)        - Adds and returns an item with the specified name and ID (optional).
//    removeItem(item)        - Remove the item (specified by its instance) from the stage.
//    clear()                 - Remove all items and clear all subscriptions. Use before deleting!
//
//    name                    - Observable, getter/setter. Name of the stage.
//    editName                - Observable, getter/setter. Whether the name is being edited.
//    toggleEditName()        - Toggles the editName property between true and false.
//
//    id()                    - Returns the ID of the stage. Read-only.
//
//    itemMoved(arg,event,ui) - Helper method for the sortable binding.
//
//    toSimpleObject()        - Returns an object representing the current state of the instance.
//
// The following methods are added to the prototype:
//
//    getNextId()             - Returns the ID that will be automatically used for the next stage.
//    setNextId(id)           - Sets the ID that will be automatically used for the next stage.
//
//    fromSimpleObject(obj)   - Returns a list instance based on the state of the object.
//
// Usage:
//
//    var stage = new (require ('tasks/Stage')) ();

define (function (require) {

   'use strict';

   var ko     = require ('knockout'  );
   var Item   = require ('tasks/Item');
   var nextId = 1;

   var Stage = function (name, setId) {

      var self = this;
      var id   = setId;

      // Set the ID to the next value unless it is already set. If it is supplied as a parameter to
      // the constructor, the value of nextId will not be changed unless it is below the supplied
      // value, in which case it will be set to this value + 1.

      if (id) {
         if (id >= nextId) {
            nextId = id + 1;
         }
      }
      else {
         id = nextId ++;
      }

      self.id             = function () { return id; };
      self.list           = null;
      self.items          = ko.observableArray ();
      self.name           = ko.observable (name);
      self.editName       = ko.observable (false);
      self.toggleEditName = function () { self.editName (! self.editName ()) };

      // Creates a new task item with the specified text and adds it to the list. Returns the new
      // task item instance. Note: The item's ID will be selected automatically.

      self.addItem = function (text, id) {
         var item = new Item (text, id);
         item.stage = self;
         self.items.push (item);
         return item;
      };

      // Removes the item (specified by the instance) from the stage. This method will cancel all
      // of the stage's subscriptions by calling stage.unsubscribe() and remove all of the stage's
      // content before removing the stage itself.

      self.removeItem = function (item) {
         item.clear ();
         self.items.remove (item);
      };

      // Subscribe to changes in the item list, and notify the task board of additions and deletions
      // (this requires the list property of the stage and the board property of the list to be set
      // to the appropriate parent instances). To unsubscribe, call the clear() method (may be a
      // good idea before deleting a stage).

      var itemsSubscription = self.items.subscribe (function (changes) {
         if (! self.list || ! self.list.board) {
            return;
         }
         changes.forEach (function (change) {
            if (change.hasOwnProperty ('moved')) {
               return;
            }
            if      (change.status === 'added'  ) { self.list.board.itemAdded   (change.value); }
            else if (change.status === 'deleted') { self.list.board.itemDeleted (change.value); }
         });
      }, null, 'arrayChange');

      // Clear the stage content and unsubscribe from all changes. This should be called before
      // deleting a stage.

      self.clear = function () {
         self.items ().forEach (function (item) {
            item.clear ();
         });
         self.items.removeAll ();
         itemsSubscription.dispose ();
      };

      // Called after drag & drop moving a task item (using the sortable binding). Update the stage
      // property of the item to the new one. See <https://github.com/rniemeyer/knockout-sortable>.

      self.itemMoved = function (arg, event, ui) { arg.item.stage = ko.dataFor (event.target) };

      // Return simple object for saving (in this case all properties except the list property
      // (since circular references will cause a problem when converting to JSON) and the editText
      // property (no point in storing that):

      self.toSimpleObject = function () {
         return {
            id       : self.id       (),
            name     : self.name     (),
            items    : self.items ().map (function (item) { return item.toSimpleObject () }),
         };
      };

   };

   // This returns a new stage instance based on the state of the specified object (one returned by
   // the toSimpleObject() method). All properties except editName will be restored. Expects the
   // object as first parameter, the parent task list instance as second parameter.

   Stage.fromSimpleObject = function (obj, list) {
      var stage = new Stage (obj.name, obj.id);
      stage.list = list;
      obj.items.forEach (function (itemObj) {
        stage.items.push (Item.fromSimpleObject (itemObj, stage));
      });
      return stage;
   };

   // Get or set the ID used for the next item instance:

   Stage.getNextId = function ()   { return nextId };
   Stage.setNextId = function (id) { nextId = id   };

   // Return the constructor:

   return Stage;

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////