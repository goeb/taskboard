// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// This module returns the constructor for task item objects. Task item instances may also be
// created using the fromSimpleObject() "static" method. A task item has the following properties:
//
//    text                    - Observable. Text of the task item.
//    stage                   - Parent task stage of the task item, null by default.
//
//    editText                - Observable. Whether the text is currently being edited.
//    toggleEditText()        - Toggles the editText observable between true and false.
//
//    id()                    - Returns the ID of the task item. Read-only.
//
//    clear()                 - Does nothing at the moment, may do something in the future…
//
//    toSimpleObject()        - Returns an object representing the current state of the instance.
//
// Prototype methods:
//
//    getNextId()             - Returns the ID that will be automatically used for the next item.
//    setNextId(id)           - Sets the ID that will be automatically used for the next item.
//
//    fromSimpleObject(obj)   - Returns a new instance based on the state of the supplied object.
//
// Usage:
//
//    var item = new (require ('tasks/Item')) ();

define (function (require) {

   'use strict';

   var ko     = require ('knockout');
   var nextId = 1;

   var Item = function (text, setId) {

      var self = this;
      var id   = setId;

      // Set the ID to the next value unless it is already set. If it is supplied as a parameter to
      // the constructor, the value of nextId will not be changed unless it is below the supplied
      // value, in which case it will be set to this value + 1.

      if (id) {
         if (id >= nextId) {
            nextId = id + 1;
         }
      }
      else {
         id = nextId ++;
      }

      self.id             = function () { return id; };
      self.stage          = null;
      self.text           = ko.observable (text );
      self.editText       = ko.observable (false);
      self.toggleEditText = function () { self.editText (! self.editText ()) };

      // Reserved for future use.

      self.clear = function () {};

      // Return simple object for saving (in this case all properties except the stage property,
      // since circular references will cause a problem when converting to JSON):

      self.toSimpleObject = function () {
         return {
            id   : self.id       (),
            text : self.text     (),
         };
      };

   };

   // This returns a new item instance based on the state of the specified object (one returned by
   // the toSimpleObject() method). All properties except editText will be restored. Expects the
   // object as first parameter, the parent stage instance as second parameter.

   Item.fromSimpleObject = function (obj, stage) {
      var item = new Item (obj.text, obj.id);
      item.stage = stage;
      return item;
   };

   // Get or set the ID used for the next item instance:

   Item.getNextId = function ()   { return nextId };
   Item.setNextId = function (id) { nextId = id   };

   // Return the constructor:

   return Item;

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////