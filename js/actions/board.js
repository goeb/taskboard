// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// This module defines some functions for the task board, including loading and saving the board.
// The module returns an object with the functions as properties. Usually functions may be called in
// different ways, for example directly as an event handler (with the event object as parameter), or
// from the command line (with the parameter array as parameter). The supported methods are listed
// for each function.
//
// Note: The actions module may be used to access all actions at once. The actions usually return
// true (or something true-ish) on success, false on failure, unless stated otherwise. For the
// functions acting both as command line and event handlers, keep in mind that the return value
// affects the event propagation.
//
// Usage:
//
//    var actions = require ('actions/board');
//    actions.saveBoard (…);

define (function (require) {

   'use strict';

   var jQuery   = require ('jquery'       );
   var config   = require ('config'       );
   var model    = require ('model'        );
   var storage  = require ('storage'      );
   var utils    = require ('utils'        );
   var defaults = require ('model/default');

   // Calculates the top margin of the main content wrapper and sets it. The new height will be the
   // height of the control area + 30px. If the user did scroll down, in addition to setting the new
   // margin the scroll position will be adjusted, so the visible part of the page will be the same.
   // May need adjustment for small scroll values…

   var setMarginAndScroll = function () {

      var doc = jQuery (document                );
      var wrp = jQuery (config.elements.wrapper );
      var ctr = jQuery (config.elements.controls);

      var pos = doc.scrollTop ();
      var old = parseInt (wrp.css ('paddingTop'), 10);

      wrp.css ('paddingTop', (ctr.outerHeight () + 20) + 'px');

      if (pos !== 0) {
         doc.scrollTop (pos - (old - ctr.outerHeight ()) + 20);
      }

   };

   // In case of failure, check if it's an event handler and if it is the beforeunload event, set
   // the output and return the appropriate value. Expects the original parameter, the output
   // message and optionally the output type (defaults to 'error') as parameters. This function is
   // only used internally in this module.

   var failure = function (params, message, type) {
      if (arguments.length < 3) {
         type = 'error';
      }
      if (utils.isEvent (params)) {
         model.controls.out (type, message);
         if (params.type === 'beforeunload') {
            return 'The board could not be saved.';
         }
         return true;
      }
      return [true, type, message];
   };

   // Save the board (including all settings etc.). Called from the command line or as an event
   // handler (for the beforeunload event to automatically save stuff when the page is closed, and
   // maybe as button click event for a save button). In case of the beforeunload event handler,
   // nothing will be returned on success (any return value would cause a confirmation message), an
   // error message on failure (this will be shown in the confirmation message on most browsers, not
   // on Firefox though, the idiots at Mozilla removed this feature).

   var saveBoard = function (params) {

      // If auto save is disabled do nothing when leaving the page:
      if (utils.isEvent (params) && params.type === 'beforeunload') {
         if (! model.controls.options.autoSave ()) {
            return;
         }
      }

      // Try to get the JSON for the model. If this fails, show the error message, and return the
      // appropriate values. Note that this only deals with exceptions thrown during the process,
      // storage module errors are handled below.
      try {
         var result = storage.save (JSON.stringify (model.toSimpleObject ()));
      }
      catch (e) {
         return failure (params, 'Error saving the board: ' + e.name + ': ' + e.message);
      }

      // Handle errors from the storage module or the storage backends:
      if (result !== true) {
         return failure (params, 'Error saving the board: ' + (result ? result : 'Unknown error.'));
      }

      // Return true on success only if not called from the beforeunload event handler:
      if (! utils.isEvent (params) || params.type !== 'beforeunload') {
         return true;
      }

   };

   // Load the board (including all settings etc.). Called from the command line, may be called from
   // an event handler, but not for the initial loading of a previsouly saved board on page load.

   var loadBoard = function (params) {

      var data = storage.load ();

      if (data.success && data.value !== null) {
         try {
            model.fromSimpleObject (jQuery.parseJSON (data.value));
         }
         catch (e) {
            return failure (params, 'Error loading the data: ' + e.name + ': ' + e.message);
         }
         return true;
      }
      else {
         var msg;
         if      (data.success) { msg = 'Could not find previsouly saved task board data.'; }
         else if (data.error  ) { msg = data.error;                                         }
         else                   { msg = 'There was an error loading the task board data.';  }
         return failure (params, msg);
      }

   };

   // Removes all the stored data, including the storage backend cookie. If auto-save is enabled, a
   // warning will be shown, in case of an error an error message will be shown. May be called from
   // the command line (parameter is an array), an event handler (parameter is an event), and from a
   // Knockout subscription (parameter is the (old or new) value).

   var removeStoredData = function (prms) {

      var result_1 = storage.remove       ();
      var result_2 = storage.clearBackend ();
      var err      = [];
      var wrn      = '';

      // Show the warning only if called from an event handler or the command line:
      if ((jQuery.isArray (prms) || utils.isEvent (prms)) && model.controls.options.autoSave ()) {
         wrn = 'Disable the auto save option to prevent the data from being saved again.';
      }

      if (result_1 !== true) { err.push (result_1 ? result_1 : 'Could not remove stored data.'); }
      if (result_2 !== true) { err.push ('Could not remove the storage backend cookie.'       ); }

      // Error messages will always override the warning:
      if (err.length) {
         return failure (prms, err.join ('<br/>'));
      }
      else {
         return wrn ? failure (prms, wrn, 'warning') : true;
      }

   };

   // Check the selected storage backend and show appropriate error messages if required. If the
   // backend (and cookies) are working, clear the output area. Only used internally in this module.
   // Expects an array as first parameter, which may contain error messages to be included in the
   // output. If a warning is set for the current storage backend, and no error occurs, the warning
   // will be displayed.

   var checkStorageBackend = function (errors) {

      var backend = storage.getBackend ();

      if (! storage.available () && backend !== 'Cookie') {
         errors.push ('The selected storage backend is not supported by your browser.');
      }

      if (! storage.available ('Cookie')) {
         errors.push ('Cookies are disabled, please check your browser options.');
      }

      if (errors.length) {
         errors.push ('Note that cookies are required regardless of the selected storage backend.');
         model.controls.out ('error', errors.join ('<br/>'));
      }
      else {
         var data = storage.instance (storage.getBackend ()).register ();
         if (data.warn) {
            model.controls.out ('warning', data.warn);
         }
         else {
            model.controls.output.clear ();
         }
      }

   };

   // Display a warning whenever auto save is switched off. This function should be called from a
   // Knockout subscription. (If auto save is switched on the output will be cleared.)

   var checkAutoSave = function (autoSave) {
      if (! autoSave) {
         model.controls.out (
            'warning', ''
            + ' Warning: You need to manually save the board for the auto save option to be'
            + ' permanent. Use the «.save» command to save the board.'
         );
      }
      else {
         model.controls.output.clear ();
      }
   };

   // Change the storage backend. The parameter must be the identifier of the new backend. This
   // function may only be called from a Knockout subscription. Doesn't return anything, clears the
   // output area on success (or displays a warning if the storage backend has one), shows an error
   // message on failure.

   var setStorageBackend = function (backend) {
      storage.setBackend (backend);
      checkStorageBackend ([]);
   };

   // Try to load a previously saved task board. If none is found, set some initial values. This
   // function is only called manually in taskboard.js, not from the command line or as an event
   // handler. This function doesn't return anything.

   var initializeBoard = function () {

      var data   = storage.load ();
      var errors = [];

      // Try to load any saved data:
      if (data.success && data.value !== null) {
         try {
            model.fromSimpleObject (jQuery.parseJSON (data.value));
         }
         catch (e) {
            errors.push ('Error loading the data: ' + e.name + ': ' + e.message);
         }
      }

      // If there was no data stored previously, or an error occured, use the default:
      if (! (data.success && data.value !== null) || errors.length) {
         model.fromSimpleObject (defaults);
      }

      checkStorageBackend (errors);

   };

   // Return an object with these functions:

   return {
      initializeBoard    : initializeBoard,
      loadBoard          : loadBoard,
      saveBoard          : saveBoard,
      setStorageBackend  : setStorageBackend,
      removeStoredData   : removeStoredData,
      setMarginAndScroll : setMarginAndScroll,
      checkAutoSave      : checkAutoSave,
   };

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////