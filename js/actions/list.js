// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// This module defined the functions to create/alter/remove task lists. The module returns an object
// with the functions as properties. Usually functions may be called in different ways, for example
// directly as an event handler (with the event object as parameter), or from the command line (with
// the parameter array as parameter). The supported methods are listed for each function.
//
// Note: The actions module may be used to access all actions at once. The actions usually return
// true (or something true-ish) on success, false on failure, unless stated otherwise. For the
// functions acting both as command line and event handlers, keep in mind that the return value
// affects the event propagation!
//
// Usage:
//
//    var actions = require ('actions/list');
//    actions.remove (…);

define (function (require) {

   'use strict';

   var jQuery = require ('jquery');
   var model  = require ('model' );
   var utils  = require ('utils' );

   // Delete a list. Called from button click event or the command line. On success and if called
   // from the command line this function returns true, else nothing.

   var remove = function (params) {
      var list = utils.getTaskList (params);
      if (list) {
         model.board.removeList (list);
         return jQuery.isArray (params) ? true : void 0;
      }
      model.controls.out ('error', 'The specified list does not exist.');
   };

   // Toggle the edit state of a list. Called from button click event or the command line. On
   // success and if called from the command line this function returns true, else nothing.

   var editName = function (params) {
      var list = utils.getTaskList (params);
      if (list) {
         list.toggleEditName ();
         utils.placeCaretAtEnd (jQuery ('[data-list-id="' + list.id () + '"] .header .name') [0]);
         return jQuery.isArray (params) ? true : void 0;
      }
      model.controls.out ('error', 'The specified list does not exist.');
   };

   // Toggle collapsed state of a task list. Called from button click event and command line. On
   // success and if called from the command line this function returns true, else nothing.

   var toggle = function (params) {
      var list = utils.getTaskList (params);
      if (list) {
         list.toggleCollapsed ();
         return jQuery.isArray (params) ? true : void 0;
      }
      model.controls.out ('error', 'The specified list does not exist.');
   };

   // Create a new task list. Only called from the command line. The parameter array must contain
   // the task list name as second element, it must not be empty. Returns either true on success or
   // false on failure. One stage will be created automatically.

   var create = function (params) {
      name = jQuery.trim (params [1]);
      if (name === '') {
         model.controls.out ('error', 'No name specified for the new task list.');
         return false;
      }
      var list = model.board.addList (name);
      if (list) {
         return !! list.addStage ('To Do');
      }
      return false;
   };

   // Return an object with these functions:

   return {
      create   : create,
      editName : editName,
      remove   : remove,
      toggle   : toggle,
   };

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////