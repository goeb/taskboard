// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// This module defined the functions to create/alter/remove task items. The module returns an object
// with the functions as properties. Usually functions may be called in different ways, for example
// directly as an event handler (with the event object as parameter), or from the command line (with
// the parameter array as parameter). The supported methods are listed for each function.
//
// Note: The actions module may be used to access all actions at once. The actions usually return
// true (or something true-ish) on success, false on failure, unless stated otherwise. For the
// functions acting both as command line and event handlers, keep in mind that the return value
// affects the event propagation!
//
// Usage:
//
//    var actions = require ('actions/item');
//    actions.remove (…);

define (function (require) {

   'use strict';

   var jQuery = require ('jquery');
   var model  = require ('model' );
   var utils  = require ('utils' );

   // Delete a task. Called from button click event or the command line. On success and if called
   // from the command line this function returns true, else nothing.

   var remove = function (params) {
      var task = utils.getTaskItem (params);
      if (task) {
         task.stage.removeItem (task);
         return jQuery.isArray (params) ? true : void 0;
      }
      model.controls.out ('error', 'The specified task does not exist.');
   };

   // Toggle the edit state of a task item. Called from button click event or the command line. On
   // success and if called from the command line this function returns true, else nothing.

   var editText = function (params) {
      var task = utils.getTaskItem (params);
      if (task) {
         task.editText (true);
         utils.placeCaretAtEnd (jQuery ('[data-item-id="' + task.id () + '"] .text') [0]);
         return jQuery.isArray (params) ? true : void 0;
      }
      model.controls.out ('error', 'The specified task does not exist.');
   };

   // Create a new task item for a list (it will be put in the first stage of the list). Called from
   // the command line or as an event handler. If called from the command line, the parameter array
   // must contain the list ID as second element, and may contain the task text as third element. On
   // success and if called from the command line this function returns true, else nothing.

   var createForList = function (params) {
      var list = utils.getTaskList (params);
      if (list) {
         if (list.stages ().length > 0) {
            var text = jQuery.isArray (params) ? jQuery.trim (params [2]) : '';
            var task = list.stages () [0].addItem (text);
            if (text === '') {
               task.text ('New Task');
               task.editText (true);
               utils.selectText (jQuery ('[data-item-id="' + task.id () + '"] .text') [0]);
            }
            jQuery ('[data-item-id="' + task.id () + '"]') [0].scrollIntoView ();
            return jQuery.isArray (params) ? true : void 0;
         }
         model.controls.out ('error', 'To add a task you need to create a stage first.');
      }
      else {
         model.controls.out ('error', 'The specified list does not exist.');
      }
   };

   // Create a new task item for a stage. Called from the command line or as an event handler. If
   // called from the command line, the parameter array must contain the stage ID as second element,
   // and may contain the task text as third element. On success and if called from the command line
   // this function returns true, else nothing.

   var createForStage = function (params) {
      var stage = utils.getTaskStage (params);
      if (stage) {
         var text = jQuery.isArray (params) ? jQuery.trim (params [2]) : '';
         var task = stage.addItem (text);
         if (text === '') {
            task.text ('New Task');
            task.editText (true);
            utils.selectText (jQuery ('[data-item-id="' + task.id () + '"] .text') [0]);
         }
         jQuery ('[data-item-id="' + task.id () + '"]') [0].scrollIntoView ();
         return jQuery.isArray (params) ? true : void 0;
      }
      model.controls.out ('error', 'The specified stage does not exist.');
   };

   // Return an object with these functions:

   return {
      createForList  : createForList,
      createForStage : createForStage,
      editText       : editText,
      remove         : remove,
   };

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////