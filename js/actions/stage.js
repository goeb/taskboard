// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// This module defined the functions to create/alter/remove task stages. The module returns an
// object with the functions as properties. Usually functions may be called in different ways, for
// example directly as an event handler (with the event object as parameter), or from the command
// line (with the parameter array as parameter). The supported methods are listed for each function.
//
// Note: The actions module may be used to access all actions at once. The actions usually return
// true (or something true-ish) on success, false on failure, unless stated otherwise. For the
// functions acting both as command line and event handlers, keep in mind that the return value
// affects the event propagation!
//
// Usage:
//
//    var actions = require ('actions/stage');
//    actions.remove (…);

define (function (require) {

   'use strict';

   var jQuery = require ('jquery');
   var model  = require ('model' );
   var utils  = require ('utils' );

   // Create a stage. May be called from the command line and as an event handler. The parameter
   // array must contain the task list ID as second element, and the new stage name as third element
   // (may be empty) if called from the command line. If called as an event handler, or if the name
   // was not set on the command line, the initial name will be set to 'New Stage', and name editing
   // will be enabled right after creating the stage. On success and if called from the command line
   // this function returns true, else nothing.

   var create = function (params) {
      var list = utils.getTaskList (params);
      if (list) {
         var name  = jQuery.isArray (params) ? jQuery.trim (params [2]) : '';
         var stage = list.addStage (name);
         if (name === '') {
            stage.name ('New Stage');
            stage.editName (true);
            utils.selectText (jQuery ('[data-stage-id="' + stage.id () + '"] .name') [0]);
         }
         return jQuery.isArray (params) ? true : void 0;
      }
      model.controls.out ('error', 'The specified list does not exist.');
   };

   // Edit a stage name. May be called from the command line or as an event handler (for both the
   // dblclick event on the name and the click event on the edit button). If called as an event
   // handler the correct editable element (either the stage header or the footer) will be focused.
   // On success and if called from the command line this function returns true, else nothing.

   var editName = function (params) {
      var stage = utils.getTaskStage (params);
      if (stage) {
         if (jQuery.isArray (params)) {
            var element = jQuery ('[data-stage-id="' + stage.id () + '"] .name');
         }
         else {
            if (params.type === 'dblclick') {
               var element = jQuery (params.target);
            }
            else {
               var element = jQuery (params.target).parents ('.header').eq (0).children ('.name');
            }
         }
         stage.editName (true);
         utils.placeCaretAtEnd (element [0]);
         return jQuery.isArray (params) ? true : void 0;
      }
      model.controls.out ('error', 'The specified stage does not exist.');
   };

   // Removes a stage. May be called from the command line or as an event handler. On success and if
   // called from the command line this function returns true, else nothing.

   var remove = function (params) {
      var stage = utils.getTaskStage (params);
      if (stage) {
         stage.list.removeStage (stage);
         return jQuery.isArray (params) ? true : void 0;
      }
      model.controls.out ('error', 'The specified stage does not exist.');
   };

   // Move a stage one position to the left or right. May be called from the command line or as an
   // event handler. On success and if called from the command line these functions return true,
   // else nothing. moveStage() is only used internally to move the stage, not called from outside
   // this module, moveLeft() and moveRight() are the actual functions to be used!

   var moveStage = function (params, move) {
      var stage = utils.getTaskStage (params);
      if (stage) {
         var list  = stage.list.stages;
         var index = list.indexOf (stage);
         list.splice (index + move < 0 ? 0 : index + move, 0, list.splice (index, 1) [0]);
         return jQuery.isArray (params) ? true : void 0;
      }
      model.controls.out ('error', 'The specified stage does not exist.');
   };

   var moveLeft  = function (params) { return moveStage (params, -1); };
   var moveRight = function (params) { return moveStage (params, +1); };

   // Return an object with these functions:

   return {
      create    : create,
      editName  : editName,
      remove    : remove,
      moveLeft  : moveLeft,
      moveRight : moveRight,
   };

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////