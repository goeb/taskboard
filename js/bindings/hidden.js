// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// The opposite of the visible binding. (Much like enable and disable, but for some reason hidden is
// not included in the Knockout core…)
//
// Example:
//
//    <input type="text" data-bind="hidden: notVisible"/>

;(function (factory) {

   // CommonJS or Node:
   if (typeof require === 'function' && typeof exports === 'object' && typeof module === 'object') {
      factory (require ('knockout'));
   }

   // AMD anonymous module:
   else if (typeof define === 'function' && define ['amd']) {
      define (['knockout'], factory);
   }

   // <script> tag:
   else {
      factory (ko);
   }

} (function (ko) {

   'use strict';

   ko.bindingHandlers.hidden = {

      update : function (element, valueAccessor, allBindings, viewModel, context) {

         var notValue = function () {
            return ! ko.unwrap (valueAccessor ());
         };

         ko.bindingHandlers.visible.update (element, notValue, allBindings, viewModel, context);

      },

   };

}));

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////