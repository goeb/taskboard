// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// Like the value binding for input elements, but this binding will update the observable on every
// input event (the input event has to be supported by the browser). Uses the original value handler
// internally, and sets the valueUpdate option to 'input'.
//
// Example:
//
//    <input type="text" data-bind="input: myObservable"/>
//
// You can still specify the valueUpdate option explicitely if required, as long as you include it
// after the input binding, for example:
//
//    <input type="text" data-bind="input: myObservable, valueUpdate: 'change'"/>
//
// This would basically restore the value binding's default bahaviour.

;(function (factory) {

   // CommonJS or Node:
   if (typeof require === 'function' && typeof exports === 'object' && typeof module === 'object') {
      factory (require ('knockout'));
   }

   // AMD anonymous module:
   else if (typeof define === 'function' && define ['amd']) {
      define (['knockout'], factory);
   }

   // <script> tag:
   else {
      factory (ko);
   }

} (function (ko) {

   'use strict';

   ko.bindingHandlers.input = {

      preprocess : function (value, name, addBinding) {
         addBinding ('valueUpdate', '"input"');
         return value;
      },

      init : function (element, valueAccessor, allBindings, viewModel, context) {
         ko.bindingHandlers.value.init (element, valueAccessor, allBindings, viewModel, context);
      },

      update : function (element, valueAccessor, allBindings, viewModel, context) {
         ko.bindingHandlers.value.update (element, valueAccessor, allBindings, viewModel, context);
      },

   };

}));

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////