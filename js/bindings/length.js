// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// Works like the enable binding, but checks the length of a string. If the supplied value is a
// string, and its length is greater than zero, the element will be enabled, if its length is zero,
// it will be disabled. If the supplied value is not a string, it is passed as is to the enable
// binding, so for everything else than strings length behaves exactly as enable.
//
// Example:
//
//    <button data-bind="length: myInput">I'm only enabled if there is input!</button>

;(function (factory) {

   // CommonJS or Node:
   if (typeof require === 'function' && typeof exports === 'object' && typeof module === 'object') {
      factory (require ('knockout'));
   }

   // AMD anonymous module:
   else if (typeof define === 'function' && define ['amd']) {
      define (['knockout'], factory);
   }

   // <script> tag:
   else {
      factory (ko);
   }

} (function (ko) {

   'use strict';

   ko.bindingHandlers.length = {

      update : function (element, valueAccessor, allBindings, viewModel, context) {

         var str    = ko.unwrap (valueAccessor ());
         var enable = typeof str === 'string' ? function () { return str.length; } : valueAccessor;

         ko.bindingHandlers.enable.update (element, enable, allBindings, viewModel, context);

      },

   };

}));

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////