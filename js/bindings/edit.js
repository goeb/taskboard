// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// Allows setting the contenteditable attribute on an element.
//
// Note: A single line of content in contenteditable elements is perfectly fine. Content with line
// breaks sucks because of browser behaviour. For now this binding will only be tested for single
// line content, it is recommended not to use multi-line content with it!
//
// If the element also has the editText binding (recommended), this handler will install an event
// listener to update the observable in real time when the element is set to be editable, and remove
// it when editing is disabled again.
//
// By default, it will also install an event handler to process the escape key, cancelling the edit
// and undoing all changes, and an event handler to process the return key, stopping the edit while
// keeping the changes. These may be disabled using the editNoEscape and editNoReturn bindings. Both
// are interpreted as boolean values, true means no handlers will be installed. Please see the note
// above before disabling the return key handler, if it is disabled return will cause a line break!
//
// Additionally, another handler may be installed for the blur event (i.e. when the element loses
// focus). The editBlur binding specifies its behaviour: if it is true, losing focus will confirm
// the changes, if it is false, the changes will be undone. In both cases the editing will be
// stopped. If it is undefined or null, this handler will not be installed, the element will stay
// contenteditable after losing the focus. This is the default.
//
// The editNoEmpty option may be set to true to prevent the content from being empty (or only
// whitespace characters). This works for the return handler, and for the blur handler if set to
// confirm the changes. The text will be set to the original text if one of these handlers would
// otherwise leave an empty or all-whitespace string. Make sure the original string is not empty
// if this option is set!
//
// Requires jQuery (and most likely a relatively modern browser for the input event). Note: For IE
// instead of input the keyup event is used!
//
// Note: When cancelling the edit all changes made since the start of the edit will be lost. This
// includes changes made by other means than editing the contenteditable element!
//
// The editText binding replaces the text handler for contenteditable elements.
//
// This handler should be used together with the edit handler (see above). It will handle updates of
// the DOM and the observable on contenteditable elements properly in real time (i.e. whenever an
// input event occurs). It will not update the DOM if an update does not change anything.
//
// Example:
//
//    <div data-bind="edit: enableEdit, editText: content, editBlur: false"></div>

;(function (factory) {

   // CommonJS or Node:
   if (typeof require === 'function' && typeof exports === 'object' && typeof module === 'object') {
      factory (require ('knockout'), require ('jquery'));
   }

   // AMD anonymous module:
   else if (typeof define === 'function' && define ['amd']) {
      define (['knockout', 'jquery'], factory);
   }

   // <script> tag:
   else {
      factory (ko, jQuery);
   }

} (function (ko, jQuery) {

   'use strict';

   ko.bindingHandlers.edit = {

      update : function (element, valueAccessor, allBindingsAccessor) {

         var editableValue = ko.unwrap (valueAccessor ());
         var editableText  = allBindingsAccessor.get ('editText');

         element = jQuery (element);
         element.attr ('contenteditable', editableValue ? 'true' : 'false');

         var eventNamespace  = '.koEditableTextHandler';
         var originalTextKey = 'koEditableTextOriginal';

         // Unconditionally remove all handlers with our namespace:
         element.off (eventNamespace);

         // If editing is disabled, remove the original text (though it may not exist), scroll to
         // the start of the element (fixes overflow: hidden behaviour for long text) and return:
         if (! editableValue) {
            element [0].scrollLeft = 0;
            element.removeData (originalTextKey);
            return;
         }

         // Install the input handler. It will update the observable bound to editText after every
         // change (i.e. unlike Knockout's default value handler, this will update in real time).
         // For IE, we use the keyup handler, input did not work properly even in IE 11.
         if (ko.isWriteableObservable (editableText)) {
            var type = navigator.userAgent.match (/msie|trident/i) ? 'keyup' : 'input';
            jQuery (element).on (type + eventNamespace, function (event) {
               editableText (element.text ());
            });
         }

         // Get the settings. See above for a description.
         var noEscapeHandler = ko.unwrap (allBindingsAccessor.get ('editNoEscape'));
         var noReturnHandler = ko.unwrap (allBindingsAccessor.get ('editNoReturn'));
         var blurHandler     = ko.unwrap (allBindingsAccessor.get ('editBlur'    ));
         var noEmpty         = ko.unwrap (allBindingsAccessor.get ('editNoEmpty' ));

         // If the original text is not yet stored on the element, store it:
         if (typeof element.data (originalTextKey) === 'undefined') {
            element.data (originalTextKey, element.text ());
         }

         // Install the keydown handler unless it has been disabled. On return, simply set the
         // contenteditable attribute to false (by setting the observable). Since the observable
         // bound to the content is updated in real time, it will already contain the new value.
         // Escape will cancel the edit by setting the edit observable to false after restoring the
         // original text. The original text stored on the element will be removed automatically
         // when editing is disabled (see above). In both cases event propagation and default
         // handlers will be disabled!
         //
         // Note: Setting contenteditable to false doesn't remove the focus from the element in
         // Firefox, so we need to call blur manually.

         if (! noEscapeHandler || ! noReturnHandler) {
            var stop = function (event) {
               valueAccessor () (false);
               element.blur ();
               event.stopPropagation ();
               event.preventDefault  ();
            };
            element.on ('keydown' + eventNamespace, function (event) {
               if (event.which === 13 && ! noReturnHandler) {
                  if (ko.isWriteableObservable (editableText)) {
                     if (noEmpty && jQuery.trim (editableText ()) === '') {
                        editableText (element.data (originalTextKey));
                     }
                  }
                  return stop (event);
               }
               if (event.which === 27 && ! noEscapeHandler) {
                  if (ko.isWriteableObservable (editableText)) {
                     editableText (element.data (originalTextKey));
                  }
                  else {
                     element.text (element.data (originalTextKey));
                  }
                  return stop (event);
               }
            });
         }

         // Install the blur handler if it is enabled:
         if (typeof blurHandler !== 'undefined' && blurHandler !== null) {
            element.on ('blur' + eventNamespace, function (event) {
               if (! blurHandler) {
                  editableText (element.data (originalTextKey));
               }
               else if (ko.isWriteableObservable (editableText)) {
                  if (noEmpty && jQuery.trim (editableText ()) === '') {
                     editableText (element.data (originalTextKey));
                  }
               }
               valueAccessor () (false);
            });
         }

      },

   };

   ko.bindingHandlers.editText = {

      update : function (element, valueAccessor) {

         var value = ko.utils.unwrapObservable (valueAccessor ());

         element = jQuery (element);

         if (element.text () !== value) {
            element.text (value);
         }

      },

   };

}));

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////