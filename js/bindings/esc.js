// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// Like the click binding, but fires when the escape key is pressed (keydown). Requires jQuery. The
// default event handlers of the browser will not be called for the event, but it will bubble up the
// DOM.
//
// Example:
//
//    <input type="text" data-bind="esc: cancelStuff"/>

;(function (factory) {

   // CommonJS or Node:
   if (typeof require === 'function' && typeof exports === 'object' && typeof module === 'object') {
      factory (require ('knockout'), require ('jquery'));
   }

   // AMD anonymous module:
   else if (typeof define === 'function' && define ['amd']) {
      define (['knockout', 'jquery'], factory);
   }

   // <script> tag:
   else {
      factory (ko, jQuery);
   }

} (function (ko, jQuery) {

   'use strict';

   ko.bindingHandlers.esc = {

      init : function (element, valueAccessor) {

         var handler = ko.unwrap (valueAccessor ());

         jQuery (element).on ('keydown', function (event) {
            if (event.which === 27) {
               handler (event);
               event.preventDefault ();
            }
         });

      },

   };

}));

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////