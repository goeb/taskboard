// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// This module returns the constructor for the Options model. The model itself has the properties:
//
//    hidden                  - Observable, getter/setter. Whether the options are currently hidden.
//    toggle()                - Toggle the hidden observable between true and false.
//
//    storageBackend          - Observable. Selected storage backend.
//    storageBackends         - Observable array. All available storage backends.
//
//    autoSave                - Observable. Whether auto save is enabled or not.
//    taskMode                - Observable. Whether task mode is enabled or not.
//
//    toSimpleObject()        - Return an object representing the current state of the instance.
//    fromSimpleObject(obj)   - Restores the state of the instance to the one represented by obj.
//
// The hidden property defaults to true.
//
// Usage:
//
//    var options = new (require ('controls/Options')) ();

define (function (require) {

   'use strict';

   var ko      = require ('knockout');
   var storage = require ('storage' );

   // Constructor:

   var Options = function () {

      var self = this;

      require ('utilities/hideable').call (self, true);

      // Storage backend related options (the backend list is fixed, but the observable array
      // doesn't hurt):
      self.storageBackend  = ko.observable      (storage.getBackend ());
      self.storageBackends = ko.observableArray (storage.backends   ());

      // Some boolean options:
      self.autoSave = ko.observable (true );
      self.taskMode = ko.observable (false);

      // Return a simple object for saving:
      self.toSimpleObject = function () {
         return {
            hidden         : self.hidden         (),
            storageBackend : self.storageBackend (),
            autoSave       : self.autoSave       (),
            taskMode       : self.taskMode       (),
         };
      };

      // Restore the state of the instance:
      self.fromSimpleObject = function (obj) {
         self.hidden         (obj.hidden        );
         self.storageBackend (obj.storageBackend);
         self.autoSave       (obj.autoSave      );
         self.taskMode       (obj.taskMode      );
      };

   };

   return Options;

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////