// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// This module returns the constructor for the Help model. The help model itself has the properties
//
//    hidden                  - Observable, boolean. Whether the help is currently hidden.
//    toggle()                - Toggle the hidden observable between true and false.
//
//    toSimpleObject()        - Return an object representing the current state of the instance.
//    fromSimpleObject(obj)   - Restores the state of the instance to the one represented by obj.
//
// The hidden property defaults to true.
//
// Usage:
//
//    var help = new (require ('controls/Help')) ();

define (function (require) {

   'use strict';

   var Help = function () {

      var self = this;

      require ('utilities/hideable').call (self, true);

      self.toSimpleObject = function () {
         return {
            hidden : self.hidden (),
         };
      };

      self.fromSimpleObject = function (obj) {
         self.hidden (obj.hidden);
      };

   };

   return Help;

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////