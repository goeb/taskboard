// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// This class bundles all the individual controls (command line, output, options and help) into one
// control model to be used as binding for the control area on the HTML page. The module returns the
// constructor function. Objects include the following properties:
//
//    commandLine             - commandLine instance, see controls/CommandLine.js.
//    help                    - Help instance,        see controls/Help.js.
//    options                 - Options instance,     see controls/Options.js.
//    output                  - Output instance,      see controls/Output.js.
//
//    out(type,text)          - Shortcut for setting output type and text.
//
//    hidden                  - Observable, getter/setter. Whether controls are hidden or not.
//    toggle()                - Toggles the hidden property between true and false.
//
//    trackChanges()          - See description below.
//
//    toSimpleObject()        - Returns an object representing the current state of the controls.
//    fromSimpleObject(obj)   - Restores the state of the instance to the one represented by obj.
//
// Usage:
//
//    var controls = new (require ('controls/Controls')) ();

define (function (require) {

   'use strict';

   var ko = require ('knockout');

   var Controls = function () {

      var self = this;

      // Add the hidden and toggle() properties:
      require ('utilities/hideable').call (self, true);

      // Add all the individual controls:
      self.commandLine = new (require ('controls/CommandLine')) ();
      self.help        = new (require ('controls/Help'       )) ();
      self.options     = new (require ('controls/Options'    )) ();
      self.output      = new (require ('controls/Output'     )) ();

      // Set the output type and text in one call. If text is not empty, show the output/controls:
      self.out = function (type, text) {
         self.output.type (type);
         self.output.text (text);
         if (text) {
            self.output.hidden (false);
            self.hidden        (false);
         }
      };

      // Return a simple object for saving the state of the controls:
      self.toSimpleObject = function () {
         return {
            hidden      : self.hidden (),
            commandLine : self.commandLine.toSimpleObject (),
            help        : self.help.toSimpleObject        (),
            options     : self.options.toSimpleObject     (),
            output      : self.output.toSimpleObject      (),
         };
      };

      // Restore the state of this instance to the state of the simple object:
      self.fromSimpleObject = function (obj) {
         self.hidden (obj.hidden);
         self.commandLine.fromSimpleObject (obj.commandLine);
         self.help.fromSimpleObject        (obj.help       );
         self.options.fromSimpleObject     (obj.options    );
         self.output.fromSimpleObject      (obj.output     );
      };

      // This computed observable will track the changes that may cause the DOM to change in a way
      // that requires some update to the board itself. Basically, this includes hiding and showing
      // some elements, and changing the text and type of the output. It does not include changes to
      // the command line! deferEvaluation is apparently required, it doesn't work properly without
      // it. For convenience, the observable itself is returned, so it can be initialized and
      // subscribed to in one step. Since the return value never changes the always notify extension
      // is required, I think…
      //
      // Usage with initialization: controls.trackChanges ().subscribe (function () { … });
      //    without initialization: controls.trackChanges.subscribe    (function () { … });

      self.trackChanges = ko.computed ({
         read : function () {
            self.hidden         ();
            self.help.hidden    ();
            self.options.hidden ();
            self.output.text    ();
            self.output.type    ();
            self.output.visible ();
            return self.trackChanges;
         },
      }).extend ({ notify : 'always', rateLimit : 1 });

   };

   return Controls;

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////