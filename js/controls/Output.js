// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// This module returns the constructor for the output model. Output objects have the following
// properties:
//
//    text                    - Observable. Contains the current output message.
//    type                    - Observable. Type of the message, may be used as CSS class.
//
//    enabled                 - Observable. To enable or disable the output.
//    hidden                  - Observable. To hide or show the output.
//    toggle()                - Toggles the hidden state between true and false.
//    visible()               - Computed observable. True if enabled is true and hidden is false.
//
//    clear()                 - Sets the empty text and type, hides the output.
//
//    toSimpleObject()        - Returns an object representing the current state of the instance.
//    fromSimpleObject(obj)   - Restores the state of the instance to the one represented by obj.
//
// Additionally, the following properties are set for the prototype:
//
//    emptyText - Default text. Will be set automatically if one attempts to set an empty message.
//    emptyType - Default type. Will be set automatically if one attempts to set an empty message.
//
// Usage:
//
//    var output = new (require ('controls/Output')) ();

define (function (require) {

   'use strict';

   var jQuery = require ('jquery'  );
   var ko     = require ('knockout');
   var config = require ('config'  );

   var Output = function () {

      var self = this;

      // Add the hidden and toggle() properties:
      require ('utilities/hideable').call (self, true);

      self.text    = ko.observable (self.emptyText);
      self.type    = ko.observable (self.emptyType);
      self.enabled = ko.observable (true);
      self.visible = ko.computed   (function () { return self.enabled () && ! self.hidden (); });

      // Clear the output and hide it:
      self.clear = function () {
         self.text   ('');
         self.hidden (true);
      };

      // Set defaults for text and type if someone tries to set an empty text:
      self.text.subscribe (function (newText) {
         if (jQuery.trim (newText) === '') {
            self.text (self.emptyText);
            self.type (self.emptyType);
         }
      });

      // Return simple object for saving:
      self.toSimpleObject = function () {
         return {
            hidden  : self.hidden  (),
            enabled : self.enabled (),
         };
      };

      // Restore the state of the instance. Text and type will not be restored and be set to the
      // empty defaults automatically by calling clear().
      self.fromSimpleObject = function (obj) {
         self.clear   ();
         self.hidden  (obj.hidden );
         self.enabled (obj.enabled);
      };

   };

   // Default text and type (defined in the configuration). Don't set these to empty strings!
   Output.prototype.emptyText = config.output.empty.text;
   Output.prototype.emptyType = config.output.empty.type;

   return Output;

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////