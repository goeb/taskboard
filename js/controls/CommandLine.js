// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// This module returns the constructor to create CommandLine instance. A CommandLine instance
// represents the command line of the application only, not any additional elements that may be
// associated with the command line, like submit buttons etc. Usually the application has exactly
// one command line. A CommandLine has the following properties:
//
//       command                 - Observable. Current text entered on the command line.
//       focus                   - Observable. True if it has the focus, else false.
//
//       clear()                 - Clears and unfocuses the command line.
//       input(text)             - Focuses the command line and sets its content to text.
//
//       exec()                  - Triggers the execute event on the command line instance.
//
//       toSimpleObject()        - Returns a simple object representing the current state.
//       fromSimpleObject(obj)   - Restores the state of the instance (see below for details).
//
// Any code that should to do something when the exec() method is called should install an event
// handler for the 'execute' event. The event will be triggered on the command line instance, not on
// any DOM element! Note that exec() will not be called automatically, if it should be called when a
// button is clicked etc., the appropriate handlers have to be created manually.
//
// Usage and exec() event example:
//
//    var cmd = new (require ('controls/CommandLine')) ();     // Create new instance.
//    jQuery (cmd).on ('execute', function (event) { … });     // Install 'execute' event handler.

define (function (require) {

   'use strict';

   var jQuery = require ('jquery'  );
   var ko     = require ('knockout');

   var CommandLine = function () {

      var self = this;

      self.command = ko.observable (''   );
      self.focus   = ko.observable (false);

      self.clear   = function ()     { self.focus (false); self.command (''  );           };
      self.input   = function (text) { self.focus (true ); self.command (text);           };

      self.exec    = function ()     { jQuery (self).trigger ('execute', self.command ()) };

      // At the moment, nothing is stored or restored:
      self.toSimpleObject   = function (   ) { return {}; };
      self.fromSimpleObject = function (obj) {            };

   };

   return CommandLine;

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////