// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// Defines and handles the commands that may be used from the command line. Both short (one letter)
// and long commands are defined in this module. The actual command handlers are implemented in the
// actions/* modules. The command line will be parsed in this module and then the appropriate
// handler will be called. This module does not return anything, simply require it to install the
// handlers for the model. Since it acts on the command line model, it does not require the DOM to
// be ready.
//
// Usage:
//
//    require ('handlers/commands');

define (function (require) {

   'use strict';

   var jQuery  = require ('jquery' );
   var actions = require ('actions');
   var model   = require ('model'  );

   // This list contains all the commands recognized by this module. Commands are specified as
   // arrays with two elements. The first element is a regular expression matching the command, the
   // second element is the command handler. Command handlers are defined in the actions/* modules.
   // The regular expression must capture any command line arguments that should be passed to the
   // handler.

   var commandHandlers = [

      // Board actions:       .save
      //                      .load
      //                      .purge

      [/^\s*\.?save\s*$/,                                   actions.board.saveBoard               ],
      [/^\s*\.?load\s*$/,                                   actions.board.loadBoard               ],
      [/^\s*\.?purge\s*$/,                                  actions.board.removeStoredData        ],

      // Task list actions:   *            <list name>
      //                      .list add    <list name>
      //                      .list edit   <list ID>
      //                      .list rm     <list ID>
      //                      .list toggle <list ID>

      [/^\s*\*\s*(.+?)\s*$/,                                actions.list.create                   ],
      [/^\s*\.?\s*list\s+add\s+(.+?)\s*$/,                  actions.list.create                   ],
      [/^\s*\.?\s*list\s+edit\s+(\d+)\s*$/,                 actions.list.editName                 ],
      [/^\s*\.?\s*list\s+toggle\s+(\d+)\s*$/,               actions.list.toggle                   ],
      [/^\s*\.?\s*list\s+rm\s+(.+?)\s*$/,                   actions.list.remove                   ],

      // Stage actions:       ~           <list ID> <stage name>
      //                      .stage add  <list ID> <stage name>
      //                      .stage edit <stage ID>
      //                      .stage rm   <stage ID>
      //                      .stage ml   <stage ID>
      //                      .stage mr   <stage ID>

      [/^\s*~\s*(\d+)\s+(.+?)\s*$/,                         actions.stage.create                  ],
      [/^\s*\.?\s*stage\s+add\s+(\d+)\s+(.+?)\s*$/,         actions.stage.create                  ],
      [/^\s*\.?\s*stage\s+edit\s+(\d+)\s*$/,                actions.stage.editName                ],
      [/^\s*\.?\s*stage\s+rm\s+(\d+)\s*$/,                  actions.stage.remove                  ],
      [/^\s*\.?\s*stage\s+ml\s+(\d+)\s*$/,                  actions.stage.moveLeft                ],
      [/^\s*\.?\s*stage\s+mr\s+(\d+)\s*$/,                  actions.stage.moveRight               ],

      // Task item actions:   #           <list ID>  <task text>
      //                      +           <stage ID> <task text>
      //                      .task new   <list ID>  <task text>
      //                      .task add   <stage ID> <task text>
      //                      .task edit  <task ID>
      //                      .task rm    <task ID>

      [/^\s*#\s*(\d+)\s+(.+?)\s*$/,                         actions.item.createForList            ],
      [/^\s*\+\s*(\d+)\s+(.+?)\s*$/,                        actions.item.createForStage           ],
      [/^\s*\.?\s*task\s+new\s+(\d+)\s+(.+?)\s*$/,          actions.item.createForList            ],
      [/^\s*\.?\s*task\s+add\s+(\d+)\s+(.+?)\s*$/,          actions.item.createForStage           ],
      [/^\s*\.?\s*task\s+edit\s+(\d+)\s*$/,                 actions.item.editText                 ],
      [/^\s*\.?\s*task\s+rm\s+(\d+)\s*$/,                   actions.item.remove                   ],

   ];

   // Look up the command, and call its handler function. If no command is found, show an error
   // message. Empty commands (or only whitespace) will be ignored. Commands will be matched against
   // the regular expressions (see above), and the result of the exec() method will be passed to the
   // command handler.
   //
   // If the handler returns true, both the command line and the output will be cleared. If the
   // handler returns false, the command line and the output will not be altered. If the command
   // handler returns an array, it must contain three elements: the first one will be interpreted as
   // boolean value, if true, the command line will be cleared, else not. The second element will be
   // used as the output type to be set, and the third element will be the new output text.

   var commandHandler = function (event, command) {

      command = jQuery.trim (command);

      if (command === '') {
         return;
      }

      for (var i = 0; i < commandHandlers.length; i ++) {
         var matches = commandHandlers [i] [0].exec (command);
         if (matches !== null) {
            var result = commandHandlers [i] [1] (matches);
            if (jQuery.isArray (result)) {
               if (result [0]) {
                  model.controls.commandLine.clear ();
               }
               model.controls.out (result [1], result [2]);
            }
            else if (result) {
               model.controls.commandLine.clear ();
               model.controls.output.clear      ();
            }
            return;
         }
      }

      model.controls.out (
         'error', 'Unknown command or syntax error. Type «.help» for more information.'
      );

   };

   // The execute event is a custom event triggered by the command lines exec() method. The event
   // handler will be called with the event object as first parameter, and the current content of
   // the command line as second parameter. Uses jQuery's event handling stuff.

   jQuery (model.controls.commandLine).on ('execute', commandHandler);

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////