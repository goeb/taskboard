// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// Installs event handlers for some DOM events. We use this instead of data-bind="click:…" etc. to
// keep the number of handlers small. The event handlers will call the appropriate functions of the
// actions/* modules. This module does not return anything, require it to install the handlers.
//
// Usage:
//
//    require ('handlers/events');

define (

   [
      'require',
      'jquery',
      'config',
      'actions',
      'domReady!'
   ],

function (require) {

   'use strict';

   var actions = require ('actions');
   var jQuery  = require ('jquery' );
   var config  = require ('config' );

   var board   = config.elements.board;

   // List header (buttons, list name):

   jQuery (board).on  ('click',      '.list > .header .btn-edit',    actions.list.editName        );
   jQuery (board).on  ('dblclick',   '.list > .header .name',        actions.list.editName        );
   jQuery (board).on  ('click',      '.list > .header .btn-toggle',  actions.list.toggle          );
   jQuery (board).on  ('click',      '.list > .header .btn-delete',  actions.list.remove          );
   jQuery (board).on  ('click',      '.list > .header .btn-stage',   actions.stage.create         );
   jQuery (board).on  ('click',      '.list > .header .btn-task',    actions.item.createForList   );

   // Stage header (buttons, stage name):

   jQuery (board).on  ('click',      '.stage.header .btn-edit',      actions.stage.editName       );
   jQuery (board).on  ('dblclick',   '.stage.header .name',          actions.stage.editName       );
   jQuery (board).on  ('click',      '.stage.header .btn-delete',    actions.stage.remove         );
   jQuery (board).on  ('click',      '.stage.header .btn-task',      actions.item.createForStage  );
   jQuery (board).on  ('click',      '.stage.header .btn-movel',     actions.stage.moveLeft       );
   jQuery (board).on  ('click',      '.stage.header .btn-mover',     actions.stage.moveRight      );

   // Task item (buttons, item text):

   jQuery (board).on  ('click',      '.item .btn-edit',              actions.item.editText        );
   jQuery (board).on  ('dblclick',   '.item .text',                  actions.item.editText        );
   jQuery (board).on  ('click',      '.item .btn-delete',            actions.item.remove          );

   // Save before leaving the page:

   jQuery (window).on ('beforeunload',                               actions.board.saveBoard      );

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////