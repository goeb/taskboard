// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// This module will register the global key handler. It does not return anything, require it to
// install the handler. The keys are defined in the configuration.
//
// Usage:
//
//    require ('handlers/keys');

define (function (require) {

   'use strict';

   var jQuery = require ('jquery');
   var config = require ('config');
   var model  = require ('model' );

   // Return and space key will be ignored if one of these elements is the target of the event, so
   // the default browser behaviour will be triggered. Note that if the event target is an element
   // with the contenteditable attribute set to true it will automatically be skipped for all keys.

   var ignoreReturn = ['a', 'button', 'input'];

   // Available handlers. Keys are assigned in the configuration.
   //
   //    toggleControls - Toggle the visiblity of the control area.
   //    toggleHelp     - Toggle the visibility of the help.
   //    toggleOutput   - Toggle the visibility of the output area.
   //    toggleOptions  - Toggle the visibility of the options area.
   //    toggleTaskMode - Toggle the task mode.
   //    focusCommand   - Focus the command line.
   //    startCommand   - Focus the command line and set the command to the key pressed.

   var handlers = {

      toggleControls : function (event) { model.controls.toggle () },

      toggleHelp     : function (event) { toggleControls (model.controls.help   ) },
      toggleOutput   : function (event) { toggleControls (model.controls.output ) },
      toggleOptions  : function (event) { toggleControls (model.controls.options) },

      toggleTaskMode : function (event) {
         model.controls.options.taskMode (! model.controls.options.taskMode ());
      },

      focusCommand   : function (event) {
         model.controls.hidden (false);
         model.controls.commandLine.focus (true);
      },

      startCommand   : function (event) {
         model.controls.hidden (false);
         model.controls.commandLine.input (String.fromCharCode (event.which));
         model.controls.commandLine.focus (true);
      },

   };

   // Toggles a control element. If the element is currently hidden or the controls are hidden, both
   // will be shown. If the element is currently shown (and the controls themenselves are visible),
   // the element will be hidden (the controls stay visible). Expects the element as first parameter
   // (this must be an instance with a hidden property).

   var toggleControls = function (element) {
      if (model.controls.hidden () || element.hidden ()) {
         model.controls.hidden (false);
         element.hidden (false);
      }
      else {
         element.hidden (true);
      }
   };

   // Handle all keypress events (if they bubble up to document), with the following exceptions:
   //
   //    * ignore all keys if Alt, Ctrl or Meta are pressed,
   //    * ignore all keys if the command line has the focus
   //    * ignore all keys if an element with contenteditable set to true has the focus,
   //    * ignore space and return keys on elements in the ignoreReturn list (see above).
   //
   // There are currently no other (input) elements that require to be ignored.
   //
   // All other keys will be looked up in the config.keys object and the appropriate handler will be
   // called. See above for the handlers. If a handler is called, the event will not be propagated
   // (though we're already at document level) and default browser behaviour for the event will be
   // disabled.

   var handleKeys = function (event) {

      if (
         event.altKey || event.ctrlKey || event.metaKey            ||
         model.controls.commandLine.focus ()                       ||
         jQuery (event.target).attr ('contenteditable') === 'true'
      ) {
         return;
      }

      // Ignore return and space keys on some elements:
      if (event.which === 13 || event.which === 32) {
         var tag = event.target.tagName.toLowerCase ();
         if (jQuery.inArray (tag, ignoreReturn) !== -1) {
            return;
         }
      }

      for (var key in config.keys) {
         if (config.keys.hasOwnProperty (key)) {
            if (parseInt (key, 10) === event.which) {
               handlers [config.keys [key]] (event);
               event.preventDefault  ();
               event.stopPropagation ();
            }
         }
      }

   };

   jQuery (document).on ('keypress', handleKeys);

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////