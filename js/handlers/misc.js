// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// This module registers some more handlers that would not really fit in the other handler modules.
// The module does not return anything, require it to install the handlers.
//
// Usage:
//
//    require ('handlers/misc');

define (function (require) {

   'use strict';

   var model   = require ('model'  );
   var actions = require ('actions');

   // Remove the stored data for the current backend:

   model.controls.options.storageBackend.subscribe (
      actions.board.removeStoredData, null, 'beforeChange'
   );

   // Set the new selected backend:

   model.controls.options.storageBackend.subscribe (
      actions.board.setStorageBackend
   );

   // Auto save disabled warning:

   model.controls.options.autoSave.subscribe (
      actions.board.checkAutoSave
   );

   // Subscribe to the controls' changes to adjust the board's top margin. Initial setting will be
   // done in taskboard.js.

   model.controls.trackChanges.subscribe (
      actions.board.setMarginAndScroll
   );

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////