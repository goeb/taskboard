// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// This module automatically loads the handlers/* modules. It does not return anything.
//
// Usage:
//
//    require ('handlers');

define (function (require) {

   'use strict';

   require ('handlers/commands');
   require ('handlers/events'  );
   require ('handlers/keys'    );
   require ('handlers/misc'    );

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////