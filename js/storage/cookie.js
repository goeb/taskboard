// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// Cookie storage backend.
//
// Storage backends must return an object with the following function properties:
//
//    save(key,value)   - Save the value under the specified key. Must return true on success. Any
//                        Other return value means failure, it may be false, or a string containing
//                        an error message.
//    load(key)         - Load the specified key. Return an object with the properties 'success' (a
//                        boolean indicating successful loading, if the key does not exist but
//                        loading would have been successful, this must be true), 'value' (in case
//                        of success the previously stored value for the key, if no value has been
//                        stored for the key, but loading would have been successful, value must be
//                        null), and 'error' (an error message in case of failure).
//    remove(key)       - Remove the specified key from the storage. Return value as for save(…).
//    available()       - Must return true if the storage backend is available, false if not.
//    register()        - Must return an object with the following properties: 'name' (the storage
//                        backend's identifier) and 'warn' (an optional warning message to be shown
//                        to the user when he selects the backend).

define (function (require) {

   'use strict';

   var cookies = require ('docCookies');

   // Load the cookie, return the value. cookies.getItem() will return null if the cookie doesn't
   // exist or can't be loaded. success and error will be set as appropriate.

   var load = function (key) {
      var a = cookies.available ();
      return {
         success : a,
         value   : cookies.getItem (key),
         error   : a ? null : 'Cookies seem to be disabled, or the size quota has been reached.',
      };
   };

   // Saves the cookie value under the specified key, and then compares the saved value to the
   // original one. If both match, returns true, else an appropriate error message. The cookies will
   // not expire (in reasonable time). Value should be a string, but this is not checked!

   var save = function (key, value) {
      cookies.setItem (key, value, Infinity);
      var stored = cookies.getItem (key);
      if (stored !== null) {
         return (
            stored.toString () === value.toString ()
            ? true
            : 'The value read does not match the one set. Maybe the size quota has been reached.'
         );
      }
      return 'Could not set the cookie. Cookies are disabled, or the size exceeds the quota.';
   };

   // Remove the data for the key. Returns either true on success or false on failure (false will
   // be returned if cookies are not available, if the key does not exist we return true, unlike the
   // cookies.removeItem() function).

   var remove = function (key) {
      if (! cookies.available ()) {
         return false;
      }
      cookies.removeItem (key);
      return cookies.getItem (key) === null;
   };

   // Returns true if cookies are enabled and working, false otherwise.

   var available = function () {
      return cookies.available ();
   };

   // register() as described at the top.

   var register = function () {
      return {
         name : 'Cookie',
         warn : 'Warning: Cookie size is limited. Saving the task board may fail!'
      };
   };

   // Return the object:

   return {
      save      : save,
      load      : load,
      remove    : remove,
      available : available,
      register  : register,
   };

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////