// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// localStorage (DOM storage) storage backend.
//
// Storage backends must return an object with the following function properties:
//
//    save(key,value)   - Save the value under the specified key. Must return true on success. Any
//                        Other return value means failure, it may be false, or a string containing
//                        an error message.
//    load(key)         - Load the specified key. Return an object with the properties 'success' (a
//                        boolean indicating successful loading, if the key does not exist but
//                        loading would have been successful, this must be true), 'value' (in case
//                        of success the previously stored value for the key, if no value has been
//                        stored for the key, but loading would have been successful, value must be
//                        null), and 'error' (an error message in case of failure).
//    remove(key)       - Remove the specified key from the storage. Return value as for save(…).
//    available()       - Must return true if the storage backend is available, false if not.
//    register()        - Must return an object with the following properties: 'name' (the storage
//                        backend's identifier) and 'warn' (an optional warning message to be shown
//                        to the user when he selects the backend).

define (function (require) {

   'use strict';

   // Load the data from localStorage. There will be an exception if something goes wrong. If the
   // key does not exist localStorage.getItem() will return null (no exception in that case).

   var load = function (key) {

      var value = null;
      var e     = null;

      try {
         value = localStorage.getItem (key);
      }
      catch (a) {
         value = null;
         e     = a;
      }

      return {
         success : e === null,
         value   : value,
         error   : e === null ? null : 'Could not load data: ' + e.name + ' — ' + e.message,
      };

   };

   // Save data to localStorage. An exception will be thrown if localStorage is not available, or
   // the data can't be saved due to the size quota. We still check if the stored value matches the
   // original one. Returns either true on success or an error message on failure.

   var save = function (key, value) {

      var stored = null;

      try {
         localStorage.setItem (key, value);
         stored = localStorage.getItem (key);
         if (stored === null || stored.toString () !== value.toString ()) {
            return 'The value read does not match. Maybe the size quota has been reached.';
         }
         return true;
      }
      catch (e) {
         return 'Could not store the data: ' + e.name + ' — ' + e.message;
      }

   };

   // Remove the data for the key. Returns either true on success or false on failure. Only an
   // exception will cause the false return value, there is no check if the key exists.

   var remove = function (key) {
      try {
         localStorage.removeItem (key);
         return localStorage.getItem (key) === null;
      }
      catch (e) {
         return false;
      }
   };

   // Return true if localStorage is available, and a test value can successfully be set and read,
   // else false.

   var available = function () {
      try {
         localStorage.setItem ('lsTest', 'Ä1A');
         var stored = localStorage.getItem ('lsTest');
         localStorage.removeItem ('lsTest');
         return stored === 'Ä1A';
      }
      catch (e) {
         return false;
      }
   };

   // register() as described at the top.

   var register = function () {
      return {
         name : 'LocalStorage',
         warn : null,
      };
   };

   // Return the object:

   return {
      save      : save,
      load      : load,
      remove    : remove,
      available : available,
      register  : register,
   };

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////