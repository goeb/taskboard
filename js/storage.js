// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// The storage frontend. This frontend's functions are called from the other modules, storage access
// is delegated to backends. The following functions are available:
//
//    save(key,value)      - Saves the key-value pair, key is optional (defaults to key()).
//    load(key)            - Returns the data for the key (optional, defaults to the key()).
//    remove(key)          - Removes the data for the key (optional, defaults to the key()).
//    key()                - Returns the key that will be used to store the board data.
//    available(backend)   - Check if the backend (optional, defaults to current one) is available.
//    setBackend(backend)  - Set the backend to be used (store its identifier in a cookie).
//    getBackend()         - Returns the current backend identifier (default if none has been set).
//    clearBackend()       - Remove the cookie that stores the current backend.
//    backends()           - Returns an array containing all backend identifiers.
//    backend(backend)     - Returns the backend instance for the specified backend identifier.
//
// Usage:
//
//    var storage = require ('storage');
//    storage.save (key, value);

define (function (require) {

   'use strict';

   var config  = require ('config'    );
   var cookies = require ('docCookies');

   // List containing the storage backend objects. Every available backend has to be added manually!

   var backendList = [
      require ('storage/cookie'      ),
      require ('storage/localstorage'),
   ];

   // List of backend object names;

   var backendNames = backendList.map (function (element) { return element.register ().name; });

   // "Hash" for backend objects. Backend names are the keys, the objects are the values:

   var backends = {};
   for (var i = 0; i < backendList.length; i ++) {
      backends [backendList [i].register ().name] = backendList [i];
   }

   // This key will be used to save the storage backend (cookie):

   var backendKey = 'tb_StorageBackend';

   // Returns the backend instance for the specified backend identifier.

   var instance = function (backend) {
      return backends [backend];
   };

   // Stores the selected backend in a cookie. The parameter has to be the backend identifier. The
   // cookie will not expire (in reasonable time), the key used is set above. Returns false if the
   // backend does not exist, else the return value of the cookies.setItem() method.

   var setBackend = function (backend) {
      if (! backends [backend]) {
         return false;
      }
      return cookies.setItem (backendKey, backend, Infinity);
   };

   // Returns the backend identifier if it has been saved, the default value otherwise. The default
   // value is set in the configuration in the storage.default property. Does not check if cookies
   // are enabled, if not, the default will be returned.

   var getBackend = function () {
      var backend = cookies.getItem (backendKey);
      return backend === null ? config.storage.default : backend;
   };

   // Remove the cookie used to store the backend. Returns true on success, false on failure. Again,
   // this does not check if cookies are enabled or not.

   var clearBackend = function () {
      if (cookies.hasItem (backendKey)) {
         cookies.removeItem (backendKey)
         return cookies.getItem (backendKey) === null;
      }
      return true;
   };

   // Returns the key that will be used to store the board. The key will contain the current page's
   // URL and a fixed prefix. If the board is stored or loaded, this key will automatically be used
   // unless another key is specified explicitely.

   var autoKey = function () {
      return 'tb_' + document.location.href;
   };

   // Save a key-value pair using the currently selected backend (as determined by getBackend()).
   // If no such backend exists, this function will return an error message. If it exists, the
   // backend's save() function will be called and its return value will be returned. If no key is
   // specified it will be determined by calling the autoKey() function. See the backends' save()
   // functions for more details. Return value will be either true on success, or false or an error
   // message (string) on failure.

   var save = function (key, value) {
      var backend = backends [getBackend ()];
      if (! backend) {
         return 'Unknown storage backend.';
      }
      return (arguments.length === 1 ? backend.save (autoKey (), key) : backend.save (key, value));
   };

   // Return the value for the supplied key using the currently selected backend (as determined by
   // getBackend()). If the backend does not exist, this function will return an appropriate error
   // object (same properties as the regular return value). If it exists, its load() function will
   // be called and the backend's return value will be returned. This will be an object with the
   // properties 'success' (a boolean indicating successful loading, if the key does not exist but
   // loading would have been successful, this will be true), 'value' (in case of success the
   // previously stored value for the key, if no value has been stored for the key, but loading
   // would have been successful, value will be null), and 'error' (an error message in case of
   // failure, may not exist on success, if it exists on success it will be null). If no key is
   // specified it will be determined by calling the autoKey() function.

   var load = function (key) {
      var backend = backends [getBackend ()];
      if (! backend) {
         return {
            success : false,
            value   : null,
            error   : 'Unknown storage backend.',
         };
      }
      return (arguments.length === 0 ? backend.load (autoKey ()) : backend.load (key));
   };

   // Delete the stored data for the specified key, using the currently selected backend. Returns
   // the return value of the backend. If no key is supplied, the autoKey() function will be used to
   // determine the key to be used. Return value will be true on success, false or an error message
   // on failure.

   var remove = function (key) {
      var backend = backends [getBackend ()];
      if (! backend) {
         return 'Unknown storage backend.';
      }
      return (arguments.length === 0 ? backend.remove (autoKey ()) : backend.remove (key));
   };

   // Checks if a backend is available, either the backend specified by its ID as the parameter, or
   // the currently selected backend if none is specified. Returns the return value of the backend's
   // available() function (true if available, false if not).

   var available = function (backendId) {
      var backend = arguments.length === 0 ? backends [getBackend ()] : backends [backendId];
      return backend && backend.available ();
   };

   // Return the storage object:

   return {
      save         : save,
      load         : load,
      remove       : remove,
      key          : autoKey,
      available    : available,
      setBackend   : setBackend,
      getBackend   : getBackend,
      clearBackend : clearBackend,
      instance     : instance,
      backends     : function () { return backendNames },
   };

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////