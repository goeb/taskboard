// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// This file is the one to be initially loaded from the index.html file with require.js like this:
//
//    <script src="require.js" data-main="initialize"></script>

require.config ({

   // Shortcuts for some libraries:

   paths                    : {
      'docCookies'          : 'libraries/docCookies',
      'domReady'            : 'libraries/domReady',
      'jquery'              : 'libraries/jquery',
      'jquery-ui'           : 'libraries/jquery-ui',
      'jquery.ui.sortable'  : 'libraries/jquery-ui',
      'knockout'            : 'libraries/knockout',
      'ko-sortable'         : 'libraries/knockout-sortable',
      'utils'               : 'utilities/utilities',
   },

   // Add dependencies for jQuery-UI stuff:

   shim                     : {
      'jquery-ui'           : {
         'deps'             : ['jquery'],
      },
      'jquery.ui.sortable'  : {
         'deps'             : ['jquery'],
      },
   },

});

// The taskboard module will automatically wait for the DOM to be ready and initialize the board:

require (['taskboard']);

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////