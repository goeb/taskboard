// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// This is the main module, it initializes all the stuff. Returns nothing.
//
// Usage:
//
//    require (['taskboard']);

define (

   [
      'require',
      'jquery',
      'knockout',
      'ko-sortable',
      'actions',
      'bindings',
      'config',
      'handlers',
      'model',
      'storage',
      'domReady!'
   ],

function (require) {

   'use strict';

   var jQuery  = require ('jquery'  );
   var ko      = require ('knockout');
   var config  = require ('config'  );
   var model   = require ('model'   );
   var storage = require ('storage' );
   var actions = require ('actions' );


   // Initialize the task board:
   actions.board.initializeBoard ();

   // Bind the task board and controls:
   ko.applyBindings (model);

   // Initialize the board's top margin:
   model.controls.trackChanges.notifySubscribers ();

   // Hide the no JavaScript warning and show the actual page:
   jQuery (config.elements.noJS    ).css ('display', 'none' );
   jQuery (config.elements.controls).css ('display', 'block');
   jQuery (config.elements.board   ).css ('display', 'block');

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////