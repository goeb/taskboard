// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// This module returns the configuration.
//
// Usage: var config = require ('config');            // Get the configuration.
//        var id     = config.elements.controls;      // Access an option.

define (function () {

   'use strict';

   return {

      // Global key handlers may be modified here. Keys are key codes, values are handler names. See
      // handlers/keys.js for a list of available handlers.

      'keys'        : {

        '13'        : 'focusCommand',           // ⏎ =  13
        '99'        : 'toggleControls',         // c =  99
       '104'        : 'toggleHelp',             // h = 104
       '111'        : 'toggleOutput',           // o = 111
       '114'        : 'focusCommand',           // r = 114
       '115'        : 'toggleOptions',          // s = 115
       '116'        : 'toggleTaskMode',         // t = 116

        '46'        : 'startCommand',           // . =  46  -  starts a long command

        '35'        : 'startCommand',           // # =  35  -  add new task (to list)
        '42'        : 'startCommand',           // * =  42  -  add new task list
        '43'        : 'startCommand',           // + =  43  -  add new task (to stage)
       '126'        : 'startCommand',           // ~ = 126  -  add a new stage

      },

      // These settings will be used to identify some DOM elements. The values have to be selectors
      // matching the elements to which the models are bound.

      'elements'    : {
         'noJS'     : '#nojs',                  // The no JavaScript message.
         'controls' : '#controls',              // Controls are bound to this element.
         'board'    : '#board',                 // The task board is bound to this element.
         'wrapper'  : '#wrapper',               // Content wrapper.
      },

      // Default text and type for an empty output area. These must not be empty strings!

      'output'      : {
         'empty'    : {
            'text'  : 'Click inside or press <kbd class="key">O</kbd> to close the output area.',
            'type'  : 'empty',
         },
      },

      // Default storage:

      'storage'     : {
         'default'  : 'LocalStorage',
      },

      // The version of the stored data format. May be used in the future to update stored data to a
      // new version if required. Do not change this unless you know what you're doing!

      dataVersion   : '1',

   };

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////