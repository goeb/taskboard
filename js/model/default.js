// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// This is the default board content/settings to be used when there is no board saved. The module
// returns the object, which has the same format as the object returned by model.toSimpleObject().
// Note that any changes in the model must be reflected in this default object! Some of the values
// are defaults retrieved from the configuration.

define (function (require) {

   var config = require ('config');

   return {

      board                               : {
         lists                            : [
            {
               id                         : 1,
               name                       : 'Tasks',
               collapsed                  : false,
               editName                   : false,
               stages                     : [
                  {
                     id                   : 1,
                     name                 : 'To Do',
                     editName             : false,
                     items                : [
                        {
                           id             : 1,
                           text           : 'Add some tasks…',
                           editText       : false
                        },
                     ],
                  },
                  {
                     id                   : 2,
                     name                 : 'In Progress',
                     editName             : false,
                     items                : [],
                  },
                  {
                     id                   : 3,
                     name                 : 'Done',
                     editName             : false,
                     items                : [],
                  }
               ]
            }
         ],
         nextListId                       : 2,
         nextStageId                      : 4,
         nextItemId                       : 2,
      },

      controls                            : {
         hidden                           : false,
         commandLine                      : {},
         help                             : {
            hidden                        : true,
         },
         options                          : {
            hidden                        : true,
            storageBackend                : config.storage.default,
            autoSave                      : true,
            taskMode                      : false,
         },
         output                           : {
            hidden                        : true,
            enabled                       : true,
         },
      },

      'dataVersion'                       : '1',

   };

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////