// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// This module automatically loads the bindings/* modules. It does not return anything.
//
// Usage:
//
//    require ('bindings');

define (function (require) {

   'use strict';

   require ('bindings/edit'  );
   require ('bindings/enter' );
   require ('bindings/esc'   );
   require ('bindings/hidden');
   require ('bindings/input' );
   require ('bindings/length');

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////