// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// This module bundles the different actions/* modules. Returns an object with all the submoules as
// properties:
//
//    board - Object containing all board actions (as defined in actions/board).
//    list  - Object containing all list  actions (as defined in actions/list ).
//    stage - Object containing all stage actions (as defined in actions/stage).
//    item  - Object containing all item  actions (as defined in actions/item ).
//
// See the individual submodules for details.
//
// Usage:
//
//    var actions = require ('actions');
//    actions.list.create (…);

define (function (require) {

   'use strict';

   return {
      board : require ('actions/board'),
      list  : require ('actions/list' ),
      stage : require ('actions/stage'),
      item  : require ('actions/item' ),
   };

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////