// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// This module will initialize the controls and board instances, and may then be used to access them
// from the other modules. The module returns an object with the controls and board properties for
// the instances. The properties are:
//
//    board                   - The board instance (see controls/Controls).
//    controls                - The controls instance (see tasks/Board).
//
//    toSimpleObject()        - Returns an object representing the current state of the model.
//    fromSimpleObject(obj)   - Restores the state of the model to that represented by obj.
//
// Usage:
//
//    var model = require ('model');

define (function (require) {

   'use strict';

   // RequireJS will call this function, the instances will be created, RequireJS will cache the
   // returned object and all subsequent calls will access the already existing cached object.

   var board    = new (require ('tasks/Board'      )) ();
   var controls = new (require ('controls/Controls')) ();
   var config   = require ('config');

   // Restore the state of the model. The parameter must be an object as returned by the
   // toSimpleObject() method. Returns nothing.

   var fromSimpleObject = function (obj) {
      controls.fromSimpleObject (obj.controls);
      board.fromSimpleObject    (obj.board   );
   };

   // Returns an object representing the state of the model. This object may be converted to a JSON
   // string (the regular model instance can not, due to circular data structures).

   var toSimpleObject = function () {
      return {
         board       : board.toSimpleObject    (),
         controls    : controls.toSimpleObject (),
         dataVersion : config.dataVersion,
      };
   };

   // Return the model object:

   return {
      board            : board,
      controls         : controls,
      fromSimpleObject : fromSimpleObject,
      toSimpleObject   : toSimpleObject,
   };

});

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////