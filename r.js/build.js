// This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
//
// Build configuration.

({

   // Paths:

   appDir                  : '../',                               // Relative to build.js.
   dir                     : '../build',                          // Relative to build.js.
   mainConfigFile          : '../js/initialize.js',               // Relative to build.js.
   baseUrl                 : 'js/',                               // Relative to appDir.

   // Optimize the almond module, include the initialize module and all its dependencies. Apparently
   // there's no way to set the output file name for a directory build, so the output it will be
   // located at build/libraries/almond.js. It has to be moved manually after the build if required.

   modules                 : [
      {
         name              : 'libraries/almond',                  // Relative to baseUrl.
         include           : ['initialize'],
         insertRequire     : ['initialize'],
      }
   ],

   // Options:

   fileExclusionRegExp     : /(^\.|^r\.js|require\.js$)/,
   optimizeCss             : 'standard',
   preserveLicenseComments : false,
   removeCombined          : true,
   useStrict               : true,
   wrap                    : true,

})

// :indentSize=3:tabSize=3:noTabs=true:mode=javascript:maxLineLen=100: /////////////////////////////