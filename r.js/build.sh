#!/bin/bash

# This file is part of Task Board. Copyright 2014 Stefan Göbel. Licensed under the GPL version 3.
#
# Build script. Requires the files r.js, compiler.jar and js.jar in the script's directory.

RDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
ROOT="$RDIR/../"
BDIR="$ROOT/build/"

# Clean up before build:

rm -rf "$BDIR"
rm -f  "$ROOT/tb-latest.tar.gz"

# Build:

java                                               \
   -classpath "$RDIR/compiler.jar":"$RDIR/js.jar"  \
   org.mozilla.javascript.tools.shell.Main         \
   "$RDIR/r.js" -o "$RDIR/build.js"

# Remove the build log:

rm -f "$BDIR/build.txt"

# Add license info and move the generated JS file, remove the libraries folder:

JS="$BDIR/js/taskboard.js"

echo '/***********************************************************************' >>"$JS"
cat  "$BDIR/LICENSES"                                                           >>"$JS"
echo '***********************************************************************/' >>"$JS"
cat  "$BDIR/js/libraries/almond.js"                                             >>"$JS"

rm -rf "$BDIR/js/libraries/"

# Modify index.html to include the generated JS file:

sed -i 's!js/libraries/require.js" data-main="js/initialize!js/taskboard.js!' "$BDIR/index.html"

# Create the .tar.gz archive containing both source and built version:

tar -czf tb-latest.tar.gz       \
   --exclude='.git'             \
   --exclude='.gitignore'       \
   --exclude='tb-latest.tar.gz' \
   --exclude='r.js/r.js'        \
   --exclude='*.jar'            \
   .

# :indentSize=3:tabSize=3:noTabs=true:mode=shell:maxLineLen=100: ###################################